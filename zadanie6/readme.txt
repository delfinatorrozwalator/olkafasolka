Napisać program ktory utworzy plik tekstowy wynik.json zawierajacy w sobie pojedynczego jsona z n kluczami gdzie n to parametr podawany przez uzytkownika
json ma wygladac nastepujaco

dla n=3
{
1: "jeden",
2: "dwa",
3: "trzy"
}

liczby skladajace się z wiecej niz jednej cyfry powinny wygladac tak

{
...
...
...
105: "jeden zero pięć",
...
...
...
}

Ważne: MA DZIAŁAĆ DLA n = 1000000

-------------------------------------
Podpowiedź:
Strukturę jsona opisaną w zadaniu można utworzyc przy pomocy np. Słownika

https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.dictionary-2?view=net-6.0

https://www.plukasiewicz.net/Artykuly/Co_nowego_w_CSharp6
