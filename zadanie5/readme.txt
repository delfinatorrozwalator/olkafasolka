Treść: Z podanego tekstu "tekst.txt" wyszukać wyszystkie słowa dłuższe niż 4 znaki zawierające literę 'a' i zapisać je w plik "output.txt" każde w osobnej linii.

Uwaga: Program powinien uruchamiać się na dowolnym komputerze z zainstalowanym pythonem (pamiętaj o używaniu ścieżek względnych)